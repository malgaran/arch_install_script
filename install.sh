#!/bin/bash


#Partition drive
sfdisk -l
read -p "On which drive do you want to install arch? (full device path): " drive
sfdisk "$drive" < ./files/partitions.sfdisk
echo "Drive configuration ended"

read -p "What is the suffix of partition 1?: " one
read -p "What is the suffix of partition 2?: " two

#Settin up disk encryption
cryptsetup luksFormat "${drive}${two}"
cryptsetup open "${drive}${two}" cryptlvm

#Setting up LVM
read -p "What size should the first logical volume be: " sizelvroot
read -p "What size should the snapshot of the first logical volume be: " sizebulvroot
read -p "What size should the snapshot of the second logical volume be: " sizebulvhome

pvcreate /dev/mapper/cryptlvm
vgcreate VG /dev/mapper/cryptlvm
lvcreate -L ${sizelvroot} VG -n root
lvcreate -L ${sizebulvroot} -s -n snapshot_root /dev/VG/root
lvcreate -l 100%FREE VG -n home
lvresize -L -${sizebulvhome} /dev/VG/home
lvcreate -l 100%FREE -s -n snapshot_home /dev/VG/home

#Format drives
mkfs.ext4 /dev/VG/root
mkfs.ext4 /dev/VG/home
mount /dev/VG/root /mnt
mount --mkdir /dev/VG/home /mnt/home
mkfs.fat -F32 ${drive}${one}
mount --mkdir ${drive}${one} /mnt/boot
ls -l /dev/disk/by-uuid/
read -p "What is the UUID of the encryptet drive? " UUID

#Install base system
pacstrap -i /mnt base --noconfirm
genfstab -U -p /mnt >> /mnt/etc/fstab
vim /mnt/etc/fstab

#Set admin password
arch-chroot /mnt passwd

#Set localization
arch-chroot /mnt ln -sf /usr/share/zoninfo/Europe/Berlin /etc/localtime
arch-chroot /mnt hwclock --systohc
cp ./files/locale.gen /mnt/etc/locale.gen
arch-chroot /mnt locale-gen
cp ./files/locale.conf /mnt/etc/locale.conf
cp ./files/vconsole.conf /mnt/etc/vconsole.conf
cp ./files/hostname /mnt/etc/hostname
cp ./files/hosts /mnt/etc/hosts

#Install packages
## linux
arch-chroot /mnt pacman -Sy --noconfirm linux linux-headers linux-firmware
## ucode & drivers
arch-chroot /mnt pacman -S --noconfirm amd-ucode mesa libva-mesa-driver
## dev
arch-chroot /mnt pacman -S --noconfirm base-devel dialog man neovim sudo git github-cli code
## connectivity
arch-chroot /mnt pacman -S --noconfirm networkmanager network-manager-applet wireless_tools wpa_supplicant wget iwd smartmontools xdg-utils firewalld nmap
## filesystem
arch-chroot /mnt pacman -S --noconfirm dosfstools lvm2 cryptsetup mtools 
## audio
arch-chroot /mnt pacman -S --noconfirm pipewire pipewire-alsa pipewire-jack pipewire-pulse gst-plugin-pipewire libpulse wireplumber
## boot manager
arch-chroot /mnt pacman -S --noconfirm efibootmgr
## kde
arch-chroot /mnt pacman -S --noconfirm plasma-meta power-profiles-daemon okular konsole dolphin ark plasma-workspace egl-wayland kcalc spectacle fwupd partitionmanager
## graphics
arch-chroot /mnt pacman -S --noconfirm xorg-server xorg-xinit

#Install additional programs
cp ./files/pacman.conf /mnt/etc/pacman.conf
arch-chroot /mnt pacman -Sy --noconfirm firefox telegram-desktop neofetch steam libreoffice-still-de vlc texlive texlive-xetex jre-openjdk texstudio minder gimp inkscape

#Install boot manager
cp ./files/mkinitcpio.conf /mnt/etc/mkinitcpio.conf
arch-chroot /mnt umount /boot
arch-chroot /mnt mount -o uid=0,gid=0,fmask=0077,dmask=0077 ${drive}${one}
arch-chroot /mnt bootctl --path=/boot install
arch-chroot /mnt mkinitcpio -p linux
echo "default arch.conf" >> /mnt/boot/loader/loader.conf
mv ./files/arch.conf /mnt/boot/loader/entries/arch.conf
echo "options rd.luks.name=${UUID}=cryptlvm root=/dev/VG/root quiet loglevel=3 systemd.show_status=auto rd.udev.log_level=3" >> /mnt/boot/loader/entries/arch.conf
arch-chroot /mnt mkinitcpio -P
arch-chroot /mnt systemctl enable sddm.service
arch-chroot /mnt systemctl enable NetworkManager.service
arch-chroot /mnt systemctl enable bluetooth.service

#Add user
arch-chroot /mnt useradd -m -g users manuel
arch-chroot /mnt passwd manuel
echo "@includedir /etc/sudoers.d" >> /mnt/etc/sudoers
echo "manuel ALL=(ALL) ALL" >> /mnt/etc/sudoers.d/00_manuel

echo done
